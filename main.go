package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

type IpInfo struct {
	Query string `json:"query"`
	Country string `json:"country"`
	City string `json:"city"`
	Region string `json:"region"`
	Isp string `json:"isp"`
}

func main(){
	var (
		ResponseObject IpInfo
		ip             string
	)

	if len(os.Args) > 1 {
		ip = os.Args[1]
	} else {
		ip = "."
	}

	url := fmt.Sprintf("http://ip-api.com/json/%s/?fields=query,country,city,region,isp", ip)

	resp, err := http.Get(url)
	if err != nil {
		log.Fatal("Could not get a API response.")
	}

	responseData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal("Could not read the response.")
	}

	json.Unmarshal(responseData, &ResponseObject)	
	fmt.Printf(
`IP: %s
Country: %s
City: %s
Region: %s
ISP: %s%c`, ResponseObject.Query, ResponseObject.Country, ResponseObject.City, ResponseObject.Region, ResponseObject.Isp, '\n')
}
