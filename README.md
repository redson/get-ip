# get-ip
Ip Grabber (13)

## Install
```bash
go install github.com/RedsonBr140/get-ip@latest
```
> Add `~/go/bin` to your `$PATH`

## Usage:
```
Usage: ip-api [STRING]...
  or:  ip-api .
Get info about an ip and print.
    If the ip is equal to nothing or ".", it is your IP.
    If the ip is equal to another ip, it'll get info about the specified ip.
```
